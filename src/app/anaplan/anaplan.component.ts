import { Component, ViewChild, OnInit } from '@angular/core';
import { PopupAnaplanComponent } from '../popup-anaplan/popup-anaplan.component';
import { MatDialog } from '@angular/material';
import { SelectionModel, DataSource } from '@angular/cdk/collections';
import { MatTableDataSource, MatSort } from '@angular/material';
import { animate, state, style, transition, trigger } from '@angular/animations';
import { AccountPreviewService } from '../salesforce/service/AccountPreview';

export interface PeriodicElement {
  Label: string;
  API_Name: string;
  Description: string;
  sync: string;
  Deployed: string;
}

export interface Elements {
  Global_VP: string;
  Geo_VP: string;
  Regional_Director: string;
  Area_Directors: string;
  Sales_Leads: string;
  Sub_States: null;
  Sales_Rep_Role: null;
  Ramping_Schedule: null;
  Suggested_Quotas: null;
  Rep_Over_Assignment: number;
  RVP_Input_Override: number;
  Final_Quota: number;
  Bookings_in_Current_Fy: number;
}

const ELEMENT_DATA: PeriodicElement[] = [
  { Label: 'Quota', API_Name: 'Quota Planning', Description: 'Brief Description of Quota Planning ', sync: '10/12/13', Deployed: 'yes' }
];


@Component({
  selector: 'app-anaplan',
  templateUrl: './anaplan.component.html',
  styleUrls: ['./anaplan.component.css'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({ height: '0px', minHeight: '0', display: 'none' })),
      state('expanded', style({ height: '*' })),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ],
})
export class AnaplanComponent implements OnInit {

  constructor(public dialog: MatDialog, private readonly accountPreviewService: AccountPreviewService) { }
  collapsed: false;

  columnsToDisplay: string[] = ['select', 'Label', 'API_Name', 'Description', 'sync', 'Deployed'];
  dataSource = new MatTableDataSource<PeriodicElement>(ELEMENT_DATA);
  expandedElement: PeriodicElement | null;
  datasources1 = [];
  displayedColumns: string[] = ['Global_VP', 'Geo_VP', 'Regional_Director', 'Area_Directors', 'Sales_Leads','Sales_Rep_Role','Ramping_Schedule','Final_Quota'];
  // datasources1 = Element_DATA1;
  // displayedColumns: string[] = ['Account_Name', 'CR_Name', 'State', 'Country', 'Account_Record'];
  // expandedElements: Elements | null;


  selection = new SelectionModel<PeriodicElement>(true, []);

  @ViewChild(MatSort) sort: MatSort;
  openDialog(): void {
    const dialogRef = this.dialog.open(PopupAnaplanComponent, {
      width: '700px',
      height: '530px'
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
    });
  }
  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }
  /** Whether the number of selected elements matches the total number of rows. */
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle() {
    this.isAllSelected() ?
      this.selection.clear() :
      this.dataSource.data.forEach(row => this.selection.select(row));
  }

  /** The label for the checkbox on the passed row */
  checkboxLabel(row?: PeriodicElement): string {
    if (!row) {
      return `${this.isAllSelected() ? 'select' : 'deselect'} all`;
    }
    return `${this.selection.isSelected(row) ? 'deselect' : 'select'} row ${row.Label + 1}`;
  }

  ngOnInit() {
    this.dataSource.sort = this.sort;
    this.accountPreviewService.getAll({})
      .subscribe(response => {
          this.datasources1 = response;
          this.displayedColumns  = ['Global_VP', 'Geo_VP', 'Regional_Director', 'Area_Directors', 'Sales_Leads','Sales_Rep_Role','Ramping_Schedule','Final_Quota'];
      });
  }

}



