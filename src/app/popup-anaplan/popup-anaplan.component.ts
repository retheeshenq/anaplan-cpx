import { Component, OnInit } from "@angular/core";
import * as _ from "lodash";

@Component({
  selector: 'app-popup-anaplan',
  templateUrl: './popup-anaplan.component.html',
  styleUrls: ['./popup-anaplan.component.css']
})
export class PopupAnaplanComponent implements OnInit {
  private show: boolean = false;
  public close(): void {
    this.show = false;
  }
  constructor() {}
  selectedObject: string = null;
  displayModelList: any[] = [];

  display: any[] = [];

  displaydesc: any[] = [];

  descriptionList: any[] = [
    {
      ObjectType:"Select",
    },
    {
      ObjectType: "Quota",
    },
  ];

  modelList: any[] = [
    {
      ObjectType: "Quota",
      Salesforce: "Global_VP",
      Anaplan: "Global_VP"
    },
    {
      ObjectType: "Quota",
      Salesforce: "Geo_VP",
      Anaplan: "Geo_VP"
    },
    {
      ObjectType: "Quota",
      Salesforce: "Regional_Director",
      Anaplan: "Regional_Director"
    },
    {
      ObjectType: "Quota",
      Salesforce: "Area_Directors",
      Anaplan: "Area_Directors"
    },
    {
      ObjectType: "Quota",
      Salesforce: "Sales_Leads",
      Anaplan: "Sales_Leads"
    },
    {
      ObjectType: "Quota",
      Salesforce: "Sales_Rep_Role",
      Anaplan: "Sales_Rep_Role"
    },
    {
      ObjectType: "Quota",
      Salesforce: "Ramping_Schedule",
      Anaplan: "Ramping_Schedule"
    },
    {
      ObjectType: "Quota",
      Salesforce: "Final_Quota",
      Anaplan: "Final_Quota"
    }
  ];
  ngOnInit() {
    this.onSelectChange();
  }
  onSelectChange() {
    if (this.selectedObject == null) {
      this.displayModelList = this.modelList;
    } else {
      this.displayModelList = _.filter(this.modelList, {
        ObjectType: this.selectedObject
      });
      console.log(this.displayModelList);
    }

    if(this.selectedObject== null){
      this.displaydesc = this.descriptionList
    }else{
    this.displaydesc = _.filter(this.descriptionList, {
      ObjectType: this.selectedObject
    });
    console.log(this.displaydesc);
  }
}
}
